﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Media;
//using System.Threading.Tasks;

namespace KazakhstanTC04
{
    public class ASU_Class {

        // Здесь будет список наших клиентов
        private Hashtable clients;
        // Это сокет нашего сервера
        Socket listener;
        // Порт, на котором будем прослушивать входящие соединения
        int port = 16789;
        // Точка для прослушки входящих соединений
        IPEndPoint Point;
        private bool isServerRunning = false;

        public Socket ASU_client;
        private bool ASU_Run = false;
        private readonly int sended = 0;

        public Thread ASU_THR;
        public bool ConnectASU = false;
        public static string Rol1_Status = "-1";
        public static string Rol2_Status = "-1";

        public void ServerStart()
        {
            try
            {
                clients = new Hashtable(30);
                listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                isServerRunning = true;
                // Определяем конечную точку, IPAddress.Any означает что наш сервер будет принимать входящие соединения с любых адресов
                Point = new IPEndPoint(IPAddress.Any, port);
                // Связываем сокет с конечной точкой
                listener.Bind(Point);
                // Начинаем слушать входящие соединения
                listener.Listen(1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            SocketAccepter();
        }

        // Этот метод будет обрабатывать входящие соединения
        private void SocketAccepter()
        {
            // Запускаем цикл в отдельном потоке, чтобы приложение не зависло
            Thread th = new Thread(delegate ()
            {
                while (isServerRunning)
                {
                    try
                    {
                        // Создаем новый сокет, по которому мы сможем обращаться клиенту
                        // Этот цикл остановится, пока какой-нибудь клиент не попытается присоединиться к серверу
                        Socket client = listener.Accept();
                        string ss = client.RemoteEndPoint.ToString();

                        int k = ss.IndexOf(':');//5 в данном случае нужный тебе символ
                        string a1 = ss.Substring(0, k);

                        if (ConnectASU == true)
                        {
                            ASU_client.Close();
                            ASU_THR.Join();
                        }
                        ASU_THR = new Thread(ASU);
                        ASU_client = client;
                        ASU_client.ReceiveTimeout = 2500;
                        ASU_THR.IsBackground = true;
                        ASU_THR.Priority = ThreadPriority.Normal;
                        ASU_THR.Start();
                        ConnectASU = true;

                    }

                    catch (Exception ex)
                    {
                        //       MessageBox.Show(ex.ToString());
                    }
                }
            });
            // Приведенный выше цикл пока что не работает, запускаем поток. Теперь цикл работает.
            th.Start();
        }

        internal void Connect()
        {            
            IPAddress ip = IPAddress.Parse("192.168.1.1");
            try
            {
                ASU_client.Connect(new IPEndPoint(ip, 16789));
                ASU_Run = true;
                Thread ASU_THR = new Thread(ASU)
                {
                    IsBackground = true,
                    Priority = ThreadPriority.AboveNormal
                };
                ASU_THR.Start();
            }
            catch (Exception e)
            {
                Console.WriteLine("\nStackTrace ---\n{0}", e.StackTrace);               
            }
        }

        private void ASU()
        {

            string StatusTC4 = "0";
            string StSush = "0";
            string StRol1 = "0";
            string StRol2 = "0";

            int Tiporazmer = 1;

            while (true)
            {
                System.Threading.Thread.Sleep(10);
                try
                {
                    byte[] bytes = new byte[10240];

                    int a = ASU_client.Receive(bytes, bytes.Length, 0);
                    if (a == 0) throw new Exception();
                    string ss = Encoding.Unicode.GetString(bytes, 0, a);
                    int i2 = 0;
                    int i = ss.IndexOf("*");
                    if (i > 0)
                    {
                        string a1 = ss.Substring(0, i);
                        switch (a1)
                        {
                            case "Status":
                                ss = ss.Remove(0, i + 1);
                                i2 = ss.IndexOf("*");

                                Rol1_Status = ss.Substring(0, i2);
                                ss = ss.Remove(0, i2 + 1);
                                i2 = ss.IndexOf("*");
                                Rol2_Status = ss.Substring(0, i2);
                                bytes = Encoding.UTF8.GetBytes("OK*");
                                ASU_client.Send(bytes);
                                /*  switch (ss.Substring(0, i2))
                                  {
                                      case "0": // Не в автомате
                                          bytes = Encoding.UTF8.GetBytes("OK*");
                                          ASU_client.Send(bytes);
                                          break;

                                      case "1": // В Автомате
                                          bytes = Encoding.UTF8.GetBytes("OK*");
                                          ASU_client.Send(bytes);
                                          break;


                                  }*/
                                break;
                        }
                    }

                    System.Threading.Thread.Sleep(10);
                }
                catch (Exception e)
                {
                    ASU_Run = false;
                    ASU_client.Close();
                    Console.WriteLine("\nStackTrace ---\n{0}", e.StackTrace);
                    Rol1_Status = "-1";
                    Rol2_Status = "-1";
                    break;
                }
            }
        }

    }
}
