﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PACNET;

namespace KazakhstanTC04
{
    internal class DIO
    {
        internal static DI di_1 = new DI(); // модули цифрового входа 0-31
        internal static DI di_2 = new DI(); // модули цифрового входа 32-63
        internal static DI di_3 = new DI(); // модули цифрового входа 64-95
        internal static DO do_1 = new DO(); // модули цифрового выхода 0-31
        internal static DO do_2 = new DO(); // модули цифрового выхода 32-63       

        internal void Load()
        {
            di_1.Init(1, 32);
            di_2.Init(2, 32);
            di_3.Init(3, 32);
            do_1.Init(4, 32);
            do_2.Init(5, 32);
            //ASU_Class ASU_Class = new ASU_Class();
            //ASU_Class.Connect();
        }

        internal void Scan()
        {
            di_1.Scan();
            di_2.Scan();
            di_3.Scan();
            //if ((Get_In(SQ12)) && (do_2.dout[KV1 - 32] == false)) { changeOUT(KV1, true); }                   
        }
        public static bool Get_In(int i)
        {
            if (i < 32) return di_1.di[i];
            else
            if (i < 64) return di_2.di[i - 32];
            else return di_3.di[i - 64];
        }

        public static bool Get_Out(int i)
        {
            if (i < 32)
                return do_1.dout[i];
            else return do_2.dout[i - 32];
        }

        public static void ChangeOUT(int i, bool b)
        {
            if (i < 32)
            {
                do_1.dout[i] = b;
                do_1.Scan();
            }
            else if (i < 64)
            {
                do_2.dout[i - 32] = b;
                do_2.Scan();
            }
            /*else
            {
                do_3.dout[i - 64] = b;
                do_3.Scan();
            }*/
        }
    }

    internal class DI
    {
        public int iChCnt;
        private int iSlot;
        public uint DI_value;
        public bool[] di;
        private IntPtr hPort;

        public void Init(int Slot, int ChCnt)
        {
            int i = 0;
            iSlot = Slot;
            iChCnt = ChCnt;
            di = new bool[iChCnt + 1];
            {
                di[i] = false;
            }
            DI_value = 0;
            hPort = PACNET.UART.Open("COM1:,115200,N,8,1");
        }

        public void Close()
        {
            PACNET.UART.Close(hPort);
        }

        public void Scan()
        {
            PACNET.PAC_IO.ReadDI(hPort, iSlot, iChCnt, ref DI_value);

            int i;
            for (i = 0; i < iChCnt; i++)
            {
                int mask = 1 << i;
                di[i] = (DI_value & mask) != 0;
            }
        }
    }
    internal class DO
    {
        public int iChCnt;
        private int iSlot;
        public uint DO_value;
        public bool[] dout;
        private IntPtr hPort;

        public void Init(int Slot, int ChCnt)
        {
            iSlot = Slot;
            iChCnt = ChCnt;
            dout = new bool[iChCnt + 1];
            short i;
            for (i = 0; i < iChCnt; i++)
            {
                dout[i] = false;
            }
            DO_value = 0;
            hPort = PACNET.UART.Open("COM1:,115200,N,8,1");
        }
        public void Close()
        {
            short i;
            for (i = 0; i < iChCnt; i++)
            {
                dout[i] = false;
            }
            for (i = 0; i < iChCnt; i++)
            {
                PACNET.PAC_IO.WriteDOBit(hPort, iSlot, iChCnt, i, 0);
            }
        }

        public void Scan()
        {
            int BitVal;
            DO_value = 0;
            int i;
            for (i = 0; i < iChCnt; i++)
            {
                int mask = 1 << i;
                if (dout[i])
                {
                    DO_value |= (uint)mask;
                }
                if (dout[i]) BitVal = 1; else BitVal = 0;
                PACNET.PAC_IO.WriteDOBit(hPort, iSlot, iChCnt, i, BitVal);
            }
        }
    }
}
