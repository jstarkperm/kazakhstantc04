﻿using System;
using System.Windows;
using System.Windows.Threading;
using System.Threading;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Collections.Generic;
using System.Reflection;
using static System.Net.Mime.MediaTypeNames;
using Image = System.Windows.Controls.Image;
using Application = System.Windows.Application;

namespace KazakhstanTC04
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static internal Window appWindow;
        private readonly SettingsWindow settingsWindow = new SettingsWindow();
        private readonly DispatcherTimer dispatcherTimer = new DispatcherTimer();
        private readonly DispatcherTimer dispatcherTimerMainUi = new DispatcherTimer();
        private readonly DispatcherTimer dispatcherTimerRolls = new DispatcherTimer();

        internal readonly DIO DIO = new DIO();
        public readonly ASU_Class asu = new ASU_Class();

        private static ListView debug11;
        private static ListView debug12;
        private static ListView debug21;
        private static ListView debug22;
        private static ListView debug31;
        private static ListView debug32;

        private Thread firstLineRunFirst = new Thread(new AutoMode.FirstLine().RunFirst);
        private Thread firstLineRunSecond = new Thread(new AutoMode.FirstLine().RunSecond);

        private Thread secondLineRunFirst = new Thread(new AutoMode.SecondLine().RunFirst);
        private Thread secondLineRunSecond = new Thread(new AutoMode.SecondLine().RunSecond);

        private Thread thirdLineRunFirst = new Thread(new AutoMode.ThirdLine().RunFirst);
        private Thread thirdLineRunSecond = new Thread(new AutoMode.ThirdLine().RunSecond);

        private List<Image> images = new List<Image>();

        public MainWindow()
        {
            DIO.Load();

            try
            {
                asu.ServerStart();
            }
            catch
            {
                MessageBox.Show("Не удалось подключиться к дефектоскопии", "Ошибка");
            }


            dispatcherTimer.Tick += DispatcherTimer_Tick;
            dispatcherTimer.Interval = TimeSpan.FromMilliseconds(10);
            dispatcherTimer.Start();

            dispatcherTimerMainUi.Tick += DispatcherTimerMainUi_Tick;
            dispatcherTimerMainUi.Interval = TimeSpan.FromMilliseconds(200);
            dispatcherTimerMainUi.Start();

            dispatcherTimerRolls.Tick += DispatcherTimerRolls_Tick;
            dispatcherTimerRolls.Interval = TimeSpan.FromMilliseconds(500);
            dispatcherTimerRolls.Start();

            InitializeComponent();
            appWindow = this;

            debug11 = this.ListView11;
            debug12 = this.ListView12;
            debug21 = this.ListView21;
            debug22 = this.ListView22;
            debug31 = this.ListView31;
            debug32 = this.ListView32;

            images.Add(S3);
            images.Add(S29);
            images.Add(S44);

            images.Add(S4);
            images.Add(S5);
            images.Add(S6);
            images.Add(S7);

            images.Add(S21);
            images.Add(S22);
            images.Add(S23);
            images.Add(S24);

            images.Add(S36);
            images.Add(S37);
            images.Add(S38);
            images.Add(S39);

            images.Add(S30);
            images.Add(S31);
            images.Add(S32);
            images.Add(S33);

            images.Add(S47);
            images.Add(S48);
            images.Add(S49);
            images.Add(S50);

            images.Add(S51);
            images.Add(S52);
            images.Add(S53);
            images.Add(S54);
        }

        private void DispatcherTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                DIO.Scan();
            }
            catch (Exception)
            {
                MessageBox.Show("Не удалось получить сигнал", "Ошибка");
                dispatcherTimer.Stop();
            }
        }

        private void DispatcherTimerMainUi_Tick(object sender, EventArgs e)
        {
            //DebugLabel.Content = ASU_Class.Rol1_Status + " " + ASU_Class.Rol2_Status;
            ConnectStatus.Fill = !ASU_Class.Rol1_Status.Equals("-1")
                ? new SolidColorBrush(Colors.Green)
                : (Brush)new SolidColorBrush(Colors.Red);

            foreach (Image image in images)
            {
                image.Source = DIO.Get_In(Int32.Parse(image.Name.Substring(1, image.Name.Length - 1)) - 1)
                    ? new BitmapImage(new Uri("src/circle_2.png", UriKind.Relative))
                    : new BitmapImage(new Uri("src/circle_1.png", UriKind.Relative));
            }

            S1.Source = DIO.Get_In(0)
                ? new BitmapImage(new Uri("src/rack_f.png", UriKind.Relative))
                : (ImageSource)new BitmapImage(new Uri("src/rack.png", UriKind.Relative));

            S25.Source = DIO.Get_In(24)
                ? new BitmapImage(new Uri("src/rack_f.png", UriKind.Relative))
                : (ImageSource)new BitmapImage(new Uri("src/rack.png", UriKind.Relative));

            S42.Source = DIO.Get_In(41)
                ? new BitmapImage(new Uri("src/rack_f.png", UriKind.Relative))
                : (ImageSource)new BitmapImage(new Uri("src/rack.png", UriKind.Relative));
        }

        private void DispatcherTimerRolls_Tick(object sender, EventArgs e)
        {
            if (DIO.do_1.dout[28] || DIO.do_1.dout[29])
            {
                if (RollFirstLine11.Source.ToString().Substring(RollFirstLine11.Source.ToString().Length - 14, 14)
                    .Equals("src/roll_1.png"))
                {
                    RollFirstLine11.Source = new BitmapImage(new Uri("src/roll_2.png", UriKind.Relative));
                    RollFirstLine12.Source = new BitmapImage(new Uri("src/roll_2.png", UriKind.Relative));
                    RollFirstLine13.Source = new BitmapImage(new Uri("src/roll_2.png", UriKind.Relative));
                }
                else
                {
                    RollFirstLine11.Source = new BitmapImage(new Uri("src/roll_1.png", UriKind.Relative));
                    RollFirstLine12.Source = new BitmapImage(new Uri("src/roll_1.png", UriKind.Relative));
                    RollFirstLine13.Source = new BitmapImage(new Uri("src/roll_1.png", UriKind.Relative));
                }
            }
            else
            {
                RollFirstLine11.Source = new BitmapImage(new Uri("src/roll_1.png", UriKind.Relative));
                RollFirstLine12.Source = new BitmapImage(new Uri("src/roll_1.png", UriKind.Relative));
                RollFirstLine13.Source = new BitmapImage(new Uri("src/roll_1.png", UriKind.Relative));
            }

            if (DIO.do_2.dout[0] || DIO.do_2.dout[1])
            {
                if (RollFirstLine21.Source.ToString().Substring(RollFirstLine21.Source.ToString().Length - 14, 14)
                .Equals("src/roll_1.png"))
                {
                    RollFirstLine21.Source = new BitmapImage(new Uri("src/roll_2.png", UriKind.Relative));
                    RollFirstLine22.Source = new BitmapImage(new Uri("src/roll_2.png", UriKind.Relative));
                    RollFirstLine23.Source = new BitmapImage(new Uri("src/roll_2.png", UriKind.Relative));
                }
                else
                {
                    RollFirstLine21.Source = new BitmapImage(new Uri("src/roll_1.png", UriKind.Relative));
                    RollFirstLine22.Source = new BitmapImage(new Uri("src/roll_1.png", UriKind.Relative));
                    RollFirstLine23.Source = new BitmapImage(new Uri("src/roll_1.png", UriKind.Relative));
                }                
            }
            else
            {
                RollFirstLine21.Source = new BitmapImage(new Uri("src/roll_1.png", UriKind.Relative));
                RollFirstLine22.Source = new BitmapImage(new Uri("src/roll_1.png", UriKind.Relative));
                RollFirstLine23.Source = new BitmapImage(new Uri("src/roll_1.png", UriKind.Relative));
            }

            if (DIO.do_1.dout[30] || DIO.do_1.dout[31])
            {
                if (RollThirdLine11.Source.ToString().Substring(RollThirdLine11.Source.ToString().Length - 14, 14)
                .Equals("src/roll_1.png"))
                {
                    RollThirdLine11.Source = new BitmapImage(new Uri("src/roll_2.png", UriKind.Relative));
                    RollThirdLine12.Source = new BitmapImage(new Uri("src/roll_2.png", UriKind.Relative));
                    RollThirdLine13.Source = new BitmapImage(new Uri("src/roll_2.png", UriKind.Relative));
                }
                else
                {
                    RollThirdLine11.Source = new BitmapImage(new Uri("src/roll_1.png", UriKind.Relative));
                    RollThirdLine12.Source = new BitmapImage(new Uri("src/roll_1.png", UriKind.Relative));
                    RollThirdLine13.Source = new BitmapImage(new Uri("src/roll_1.png", UriKind.Relative));
                }
            } 
            else
            {
                RollThirdLine11.Source = new BitmapImage(new Uri("src/roll_1.png", UriKind.Relative));
                RollThirdLine12.Source = new BitmapImage(new Uri("src/roll_1.png", UriKind.Relative));
                RollThirdLine13.Source = new BitmapImage(new Uri("src/roll_1.png", UriKind.Relative));
            }

            if (DIO.do_2.dout[2] || DIO.do_2.dout[3])
            {
                if (RollThirdLine21.Source.ToString().Substring(RollThirdLine21.Source.ToString().Length - 14, 14)
                .Equals("src/roll_1.png"))
                {
                    RollThirdLine21.Source = new BitmapImage(new Uri("src/roll_2.png", UriKind.Relative));
                    RollThirdLine22.Source = new BitmapImage(new Uri("src/roll_2.png", UriKind.Relative));
                    RollThirdLine23.Source = new BitmapImage(new Uri("src/roll_2.png", UriKind.Relative));
                }
                else
                {
                    RollThirdLine21.Source = new BitmapImage(new Uri("src/roll_1.png", UriKind.Relative));
                    RollThirdLine22.Source = new BitmapImage(new Uri("src/roll_1.png", UriKind.Relative));
                    RollThirdLine23.Source = new BitmapImage(new Uri("src/roll_1.png", UriKind.Relative));
                }
            }
            else
            {
                RollThirdLine21.Source = new BitmapImage(new Uri("src/roll_1.png", UriKind.Relative));
                RollThirdLine22.Source = new BitmapImage(new Uri("src/roll_1.png", UriKind.Relative));
                RollThirdLine23.Source = new BitmapImage(new Uri("src/roll_1.png", UriKind.Relative));
            }

        }

        private void SettingsButton_Click(object sender, RoutedEventArgs e)
        {
            settingsWindow.Show();
            settingsWindow.dispatcherTimer.Start();
        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            firstLineRunFirst.Abort();
            firstLineRunSecond.Abort();
            secondLineRunFirst.Abort();
            secondLineRunSecond.Abort();
            thirdLineRunFirst.Abort();
            thirdLineRunSecond.Abort();

            try
            {
                DIO.do_1.Close();
                DIO.do_2.Close();
                DIO.di_1.Close();
                DIO.di_2.Close();
                DIO.di_3.Close();
            }
            catch (Exception)
            {
                //MessageBox.Show("Не удалось отправить сигнал", "Ошибка");                
            }
            Application.Current.Shutdown();
            System.Diagnostics.Process.GetCurrentProcess().Kill(); // Остановка всех потоков
        }

        private void AutoButton_Click(object sender, RoutedEventArgs e)
        {
            if (AutoButtonTextBlock.Text.Contains("Автоматический"))
            {

                FirstLineStartButton_MouseLeftButtonDown(FirstLineStartButton, null);
                SecondLineStartButton_MouseLeftButtonDown(SecondLineStartButton, null);
                ThirdLineStartButton_MouseLeftButtonDown(ThirdLineStartButton, null);

                AutoButtonTextBlock.Text = "Стоп";
                return;
            }

            FirstLineStopButton_MouseLeftButtonDown(FirstLineStopButton, null);
            SecondLineStopButton_MouseLeftButtonDown(SecondLineStopButton, null);
            ThirdLineStopButton_MouseLeftButtonDown(ThirdLineStopButton, null);

            try
            {
                DIO.do_1.Close();
                DIO.do_2.Close();
            }
            catch
            {
                //MessageBox.Show("Не удалось отправить сигнал", "Ошибка"); 
            }
            AutoButtonTextBlock.Text = "Автоматический\nрежим";
        }

        private void DebugButton_Click(object sender, RoutedEventArgs e)
        {
            if (DebugButton.Content.Equals("Отладка"))
            {
                GridListView.Visibility = Visibility.Visible;
                DebugButton.Content = "Спрятать";
                return;
            }

            GridListView.Visibility = Visibility.Hidden;
            DebugButton.Content = "Отладка";
        }

        private void FirstLineStartButton_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (!firstLineRunFirst.IsAlive)
            {
                firstLineRunFirst = new Thread(new AutoMode.FirstLine().RunFirst);
                firstLineRunSecond = new Thread(new AutoMode.FirstLine().RunSecond);
                firstLineRunFirst.Start();
                firstLineRunSecond.Start();
            }

            ((Image)sender).Source = new BitmapImage(new Uri("src/start_p.png", UriKind.Relative));
            FirstLineStopButton.Source = new BitmapImage(new Uri("src/stop.png", UriKind.Relative));
            ((Image)sender).IsEnabled = false;
            FirstLineStopButton.IsEnabled = true;
        }

        private void FirstLineStopButton_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            // Рольганги стоп
            DIO.ChangeOUT(28, false);
            DIO.ChangeOUT(29, false);
            DIO.ChangeOUT(32, false);
            DIO.ChangeOUT(33, false);

            DIO.ChangeOUT(22, false); // Установка правки стоп

            firstLineRunFirst.Abort();
            firstLineRunSecond.Abort();
            MainWindow.AddDebugRecord("Линия правки остановлена (1)", 11);
            MainWindow.AddDebugRecord("Линия правки остановлена (2)", 12);

            ((Image)sender).Source = new BitmapImage(new Uri("src/stop_p.png", UriKind.Relative));
            FirstLineStartButton.Source = new BitmapImage(new Uri("src/start.png", UriKind.Relative));
            ((Image)sender).IsEnabled = false;
            FirstLineStartButton.IsEnabled = true;
        }

        private void SecondLineStartButton_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (!secondLineRunFirst.IsAlive)
            {
                secondLineRunFirst = new Thread(new AutoMode.SecondLine().RunFirst);
                secondLineRunSecond = new Thread(new AutoMode.SecondLine().RunSecond);
                secondLineRunFirst.Start();
                secondLineRunSecond.Start();
            }

            ((Image)sender).Source = new BitmapImage(new Uri("src/start_p.png", UriKind.Relative));
            SecondLineStopButton.Source = new BitmapImage(new Uri("src/stop.png", UriKind.Relative));
            ((Image)sender).IsEnabled = false;
            SecondLineStopButton.IsEnabled = true;
        }

        private void SecondLineStopButton_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            secondLineRunFirst.Abort();
            secondLineRunSecond.Abort();
            MainWindow.AddDebugRecord("Линия дефектоскопии остановлена (1)", 21);
            MainWindow.AddDebugRecord("Линия дефектоскопии остановлена (2)", 22);

            ((Image)sender).Source = new BitmapImage(new Uri("src/stop_p.png", UriKind.Relative));
            SecondLineStartButton.Source = new BitmapImage(new Uri("src/start.png", UriKind.Relative));
            ((Image)sender).IsEnabled = false;
            SecondLineStartButton.IsEnabled = true;
        }

        private void ThirdLineStartButton_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (!thirdLineRunFirst.IsAlive)
            {
                thirdLineRunFirst = new Thread(new AutoMode.ThirdLine().RunFirst);
                thirdLineRunSecond = new Thread(new AutoMode.ThirdLine().RunSecond);
                thirdLineRunFirst.Start();
                thirdLineRunSecond.Start();
            }

            ((Image)sender).Source = new BitmapImage(new Uri("src/start_p.png", UriKind.Relative));
            ThirdLineStopButton.Source = new BitmapImage(new Uri("src/stop.png", UriKind.Relative));
            ((Image)sender).IsEnabled = false;
            ThirdLineStopButton.IsEnabled = true;
        }

        private void ThirdLineStopButton_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            // Рольганги стоп
            DIO.ChangeOUT(30, false);
            DIO.ChangeOUT(31, false);
            DIO.ChangeOUT(34, false);
            DIO.ChangeOUT(35, false);

            thirdLineRunFirst.Abort();
            thirdLineRunSecond.Abort();
            MainWindow.AddDebugRecord("Линия отреза остановлена (1)", 31);
            MainWindow.AddDebugRecord("Линия отреза остановлена (2)", 32);

            ((Image)sender).Source = new BitmapImage(new Uri("src/stop_p.png", UriKind.Relative));
            ThirdLineStartButton.Source = new BitmapImage(new Uri("src/start.png", UriKind.Relative));
            ((Image)sender).IsEnabled = false;
            ThirdLineStartButton.IsEnabled = true;
        }

        public static void AddDebugRecord(String record, short number)
        {
            switch (number)
            {
                case 11:
                    {
                        debug11.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                            (ThreadStart)delegate ()
                                {
                                    debug11.Items.Add(record);
                                }
                            );
                        break;
                    }
                case 12:
                    {
                        debug12.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                            (ThreadStart)delegate ()
                            {
                                debug12.Items.Add(record);
                            }
                            );
                        break;
                    }
                case 21:
                    {
                        debug21.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                            (ThreadStart)delegate ()
                            {
                                debug21.Items.Add(record);
                            }
                            );
                        break;
                    }
                case 22:
                    {
                        debug22.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                            (ThreadStart)delegate ()
                            {
                                debug22.Items.Add(record);
                            }
                            );
                        break;
                    }
                case 31:
                    {
                        debug31.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                            (ThreadStart)delegate ()
                            {
                                debug31.Items.Add(record);
                            }
                            );
                        break;
                    }
                case 32:
                    {
                        debug32.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                            (ThreadStart)delegate ()
                            {
                                debug32.Items.Add(record);
                            }
                            );
                        break;
                    }
            }
        }

        private void FirstLineTransitButton_Click(object sender, RoutedEventArgs e)
        {
            if (!AutoMode.ThirdLineTrasit)
            {
                ((Button)sender).Style = (Style)FindResource("buttonStyleDoPressed");
                AutoMode.ThirdLineTrasit = true;
                return;
            }
             ((Button)sender).Style = (Style)FindResource("buttonStyleDoDefault");
            AutoMode.ThirdLineTrasit = false;
        }
    }
}
