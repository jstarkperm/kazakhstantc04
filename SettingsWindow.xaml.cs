﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;

namespace KazakhstanTC04
{
    /// <summary>
    /// Interaction logic for SettingsWindow.xaml
    /// </summary>
    public partial class SettingsWindow : Window
    {
        internal DispatcherTimer dispatcherTimer = new DispatcherTimer();

        public SettingsWindow()
        {
            InitializeComponent();
            dispatcherTimer.Tick += DispatcherTimer_Tick;
            dispatcherTimer.Interval = TimeSpan.FromMilliseconds(100);
            dispatcherTimer.Start();
        }

        private void SetDO(Button b, int i)
        {
            if (DIO.Get_Out(i).Equals(false))
            {                
                try
                {
                    DIO.ChangeOUT(i, true);
                }
                catch (Exception)
                {
                    MessageBox.Show("Не удалось отправить сигнал", "Ошибка");
                    return;
                }
                b.Style = (Style)b.FindResource("buttonStyleDoPressed");
                return;
            }
            try
            {
                DIO.ChangeOUT(i, false);
            }
            catch (Exception)
            {
                MessageBox.Show("Не удалось отправить сигнал", "Ошибка"); ;
                return;
            }
            b.Style = (Style)b.FindResource("buttonStyleDoDefault");
        }

        private void ButtonClick(object sender, RoutedEventArgs e)
        {
            int doValue = Int32.Parse(((Button)sender).Name.Substring(1, ((Button)sender).Name.Length - 1)) - 1;
            SetDO((Button)sender, doValue);
        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DIO.do_1.Close();
                DIO.do_2.Close();
            }
            catch (Exception)
            {
                //MessageBox.Show("Не удалось отправить сигнал", "Ошибка");
            }
            dispatcherTimer.Stop();
            MainWindow.appWindow.Show();
            this.Hide();
        }

        private void DispatcherTimer_Tick(object sender, EventArgs e)
        {
            if (diTabItem1.IsSelected)
            {
                ChangeBackgroudListBoxItem(diList1);
            }
            if (diTabItem2.IsSelected)
            {
                ChangeBackgroudListBoxItem(diList2);
            }
            if (diTabItem3.IsSelected)
            {
                ChangeBackgroudListBoxItem(diList3);
            }
        }

        private void ChangeBackgroudListBoxItem(ListBox listBox)
        {
            foreach (ListBoxItem item in listBox.Items)
            {
                int itemNameNumber = Int32.Parse(item.Name.Substring(1, item.Name.Length - 1)) - 1;
                item.Background = DIO.Get_In(itemNameNumber).Equals(true) ? new SolidColorBrush(Colors.Lime) : new SolidColorBrush(Colors.IndianRed);
            }
        }       
    }
}
