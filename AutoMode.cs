﻿using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Threading;
using System.Windows;

namespace KazakhstanTC04
{
    class AutoMode
    {
        public static bool ThirdLineTrasit = false;

        public class FirstLine
        {
            public void RunFirst()
            {
                MainWindow.AddDebugRecord("Линия правки старт (1)", 11);

                while (true)
                {

                    // Проверка перекладчика в исходном или нет
                    if (!DIO.Get_In(S8))
                    {
                        DIO.ChangeOUT(K2, true);
                        Thread.Sleep(200);
                        DIO.ChangeOUT(K2, false);
                    }
                    MainWindow.AddDebugRecord("Ожидание наличия штанги на стеллаж перед правкой", 11);
                    while (!DIO.Get_In(S3)) { Thread.Sleep(10); } // Наличие прутка на стеллажe перед рольгангом правки
                    MainWindow.AddDebugRecord("Проверка есть ли пруток на рольганге", 11);
                    while (DIO.Get_In(S4) || DIO.Get_In(S5) || DIO.Get_In(S6) || DIO.Get_In(S7)) { Thread.Sleep(10); } // Пруток на линии правки 1 2 3
                    MainWindow.AddDebugRecord("Пруток на рольганге отсутсвует", 11);
                    MainWindow.AddDebugRecord("Наличие штанги на стеллаже перед правкой", 11);
                    Thread.Sleep(1000);
                    // Перекладчик на рольганг перед правкой в рабочее
                    DIO.ChangeOUT(K1, true);
                    Thread.Sleep(200);
                    DIO.ChangeOUT(K1, false);
                    MainWindow.AddDebugRecord("Ожидание перекладчика в рабочее", 11);
                    while (!DIO.Get_In(S9)) { Thread.Sleep(10); }
                    MainWindow.AddDebugRecord("Перекладчик в рабочем", 11);
                    // Перекладчик на рольганг перед правкой в исходное 
                    DIO.ChangeOUT(K2, true);
                    Thread.Sleep(200);
                    DIO.ChangeOUT(K2, false);
                    MainWindow.AddDebugRecord("Ожидание перекладчика в исходное", 11);
                    while (!DIO.Get_In(S9)) { Thread.Sleep(10); }
                    while (!DIO.Get_In(S8)) { Thread.Sleep(10); }
                    MainWindow.AddDebugRecord("Перекладчик в исходном", 11);
                    Thread.Sleep(1000);
                }
            }
            public void RunSecond()
            {
                MainWindow.AddDebugRecord("Линия правки старт (2)", 12);
                while (true)
                {
                    // Проверка перекладчика в исходном или нет
                    if (!DIO.Get_In(S27))
                    {
                        DIO.ChangeOUT(K5, true);
                        Thread.Sleep(200);
                        DIO.ChangeOUT(K5, false);
                    }
                    short position = 1;
                    if (DIO.Get_In(S21) && !DIO.Get_In(S24)) position = 2;
                    if (DIO.Get_In(S24)) position = 3;
                    switch (position)
                    {
                        case 1:
                            {
                                MainWindow.AddDebugRecord("Ожидание прутка на рольганге перед правкой", 12);
                                while (!DIO.Get_In(S4) && !DIO.Get_In(S5) && !DIO.Get_In(S6)) { Thread.Sleep(10); } // Пруток на линии правки 1 2 3
                                MainWindow.AddDebugRecord("Пруток на рольганге перед правкой", 12);
                                MainWindow.AddDebugRecord("Ожидание сброса прутка на стеллаж после правки", 12);
                                while (DIO.Get_In(S22) || DIO.Get_In(S23) || DIO.Get_In(S24)) { Thread.Sleep(10); } // Пруток на линии правки 1 2 3
                                MainWindow.AddDebugRecord("Пруток сброшен", 12);
                                DIO.ChangeOUT(K23, true); // Установка правки пуск                     
                                DIO.ChangeOUT(K29, true); // Рольганг перед правкой вперёд                    
                                DIO.ChangeOUT(K33, true); // Рольганг после правки вперёд 
                                MainWindow.AddDebugRecord("Ожидание вкючения датчика перед устновкой правки", 12);
                                while (!DIO.Get_In(S7)) { Thread.Sleep(10); } // Пруток на рольганге после правки 4
                                MainWindow.AddDebugRecord("Датчика перед устновкой правки включен", 12);
                                MainWindow.AddDebugRecord("Ожидание выключения датчика перед устновкой правки", 12);
                                while (DIO.Get_In(S7)) { Thread.Sleep(10); } // Пруток на рольганге после правки 4
                                MainWindow.AddDebugRecord("Датчика перед устновкой правки выключен", 12);
                                DIO.ChangeOUT(K29, false); // Рольганг перед правкой стоп

                                Work();

                                break;
                            }
                        case 2:
                            {
                                DIO.ChangeOUT(K23, true); // Установка правки пуск
                                DIO.ChangeOUT(K29, true); // Рольганг перед правкой вперёд
                                DIO.ChangeOUT(K33, true); // Рольганг после правки вперёд
                                while (DIO.Get_In(S7)) { Thread.Sleep(10); }
                                DIO.ChangeOUT(K29, false); // Рольганг перед правкой стоп

                                Work();

                                break;
                            }
                        case 3:
                            {
                                Work();
                                break;
                            }
                    }

                }

            }

            private void Work()
            {
                MainWindow.AddDebugRecord("Пруток в установке правки", 12);
                MainWindow.AddDebugRecord("Ожидание прутка на выходе из установки правки", 12);
                while (!DIO.Get_In(S24)) { Thread.Sleep(10); } // Пруток на рольганге после правки 3            
                MainWindow.AddDebugRecord("Пруток в упоре", 12);
                Thread.Sleep(1500);
                DIO.ChangeOUT(K23, false); // Установка правки стоп                    
                DIO.ChangeOUT(K33, false); // Рольганг после правки стоп             
                if (DIO.Get_In(S25) || DIO.Get_In(S26)) MainWindow.AddDebugRecord("Накопительный стеллаж заполнен", 12); //Накопительный стеллаж перед дефектоскопией переполнен
                while (DIO.Get_In(S25) || DIO.Get_In(S26)) { Thread.Sleep(10); }
                // Перекладчик с рольганга правки в рабочее
                DIO.ChangeOUT(K6, true);
                Thread.Sleep(200);
                DIO.ChangeOUT(K6, false);
                //
                while (!DIO.Get_In(S28)) { Thread.Sleep(10); }
                MainWindow.AddDebugRecord("Перекладчик в рабочем", 12);
                Thread.Sleep(2000);
                // Перекладчик с рольганга правки в исходное 
                DIO.ChangeOUT(K5, true);
                Thread.Sleep(200);
                DIO.ChangeOUT(K5, false);
                //
            }
        }

        public class SecondLine
        {
            public void RunFirst()
            {
                if (DIO.Get_In(S35))
                {
                    // Перекладчик на рольганг дефектоскопии в исходное
                    DIO.ChangeOUT(K7, true);
                    Thread.Sleep(200);
                    DIO.ChangeOUT(K7, false);
                    while (!DIO.Get_In(S34)) { Thread.Sleep(10); } // Ждем перекладчик в рабочее
                }
                MainWindow.AddDebugRecord("Линия дефектоскопии старт (1)", 21);
                while (true)
                {
                    while (!ASU_Class.Rol1_Status.Equals("1")) { Thread.Sleep(10); } // Ждем сигнал от дефектоскопии
                    Thread.Sleep(1000);
                    MainWindow.AddDebugRecord("Ожидание прутка на стеллаже перед дефектоскопией", 21);
                    while (!DIO.Get_In(S29)) { Thread.Sleep(10); } // Ждем пруток на стеллаже
                    MainWindow.AddDebugRecord("Ожидание освобождения рольганга", 21);
                    while (DIO.Get_In(S31) || DIO.Get_In(S32) || DIO.Get_In(S33)) { Thread.Sleep(10); } // Ждем пруток на стеллаже
                    // Перекладчик на рольганг дефектоскопии в рабочее 
                    DIO.ChangeOUT(K8, true);
                    Thread.Sleep(200);
                    DIO.ChangeOUT(K8, false);
                    // Перекладчик на рольганг дефектоскопии в исходное
                    while (!DIO.Get_In(S35)) { Thread.Sleep(10); } // Ждем перекладчик в рабочее
                    DIO.ChangeOUT(K7, true);
                    Thread.Sleep(200);
                    DIO.ChangeOUT(K7, false);
                    while (!ASU_Class.Rol1_Status.Equals("0")) { Thread.Sleep(10); } // Ждем сигнал от дефектоскопии
                }

            }

            public void RunSecond()
            {
                // Проверка перекладячиков в исходном или нет
                if (DIO.Get_In(S58))
                {
                    // Перекладчик на рольганг после дефектоскопии в исходное
                    DIO.ChangeOUT(K16, true);
                    Thread.Sleep(200);
                    DIO.ChangeOUT(K16, false);
                    while (!DIO.Get_In(S57)) { Thread.Sleep(10); } // Ждем перекладчик в рабочее
                }

                if (DIO.Get_In(S40))
                {
                    // Перекладчик брака дефектоскопии в исходное
                    DIO.ChangeOUT(K10, true);
                    Thread.Sleep(200);
                    DIO.ChangeOUT(K10, false);
                    while (!DIO.Get_In(S41)) { Thread.Sleep(10); } // Ждем перекладчик в рабочее
                }

                MainWindow.AddDebugRecord("Линия дефектоскопии старт (2)", 22);
                while (true)
                {
                    MainWindow.AddDebugRecord("Ждем пруток на рольганге", 22);
                    while (!DIO.Get_In(S36) || !DIO.Get_In(S37)) { Thread.Sleep(10); } // Ждем датчиков на рольганге стеллаже
                    while (ASU_Class.Rol2_Status.Equals("0")) { Thread.Sleep(10); }
                    if (ASU_Class.Rol2_Status.Equals("1"))
                    {
                        MainWindow.AddDebugRecord("Товар", 22);
                        // Перекладчик дефектоскопии в исходное
                        DIO.ChangeOUT(K15, true);
                        Thread.Sleep(200);
                        DIO.ChangeOUT(K15, false);
                        while (!DIO.Get_In(S58)) { Thread.Sleep(10); } // Ждем перекладчик в исходное
                        Thread.Sleep(2000);
                        DIO.ChangeOUT(K16, true);
                        Thread.Sleep(200);
                        DIO.ChangeOUT(K16, false);
                        while (!DIO.Get_In(S57)) { Thread.Sleep(10); } // Ждем перекладчик в рабочее
                    }

                    if (ASU_Class.Rol2_Status.Equals("2"))
                    {
                        MainWindow.AddDebugRecord("Брак", 22);
                        // Перекладчик брака дефектоскопии в исходное
                        DIO.ChangeOUT(K9, true);
                        Thread.Sleep(200);
                        DIO.ChangeOUT(K9, false);
                        while (!DIO.Get_In(S40)) { Thread.Sleep(10); } // Ждем перекладчик в исходное
                        Thread.Sleep(2000);
                        DIO.ChangeOUT(K10, true);
                        Thread.Sleep(200);
                        DIO.ChangeOUT(K10, false);
                        while (!DIO.Get_In(S41)) { Thread.Sleep(10); } // Ждем перекладчик в рабочее
                    }
                }
            }
        }


        public class ThirdLine
        {
            public void RunFirst()
            {
                MainWindow.AddDebugRecord("Линия отреза старт (1)", 31);
                while (true)
                {
                    // Проверка перекладчика в исходном или нет
                    if (!DIO.Get_In(S45))
                    {
                        DIO.ChangeOUT(K12, true);
                        Thread.Sleep(200);
                        DIO.ChangeOUT(K12, false);
                    }

                    MainWindow.AddDebugRecord("Ожидание прутка на стеллаже перед отрезкой", 31);
                    while (!DIO.Get_In(S44)) { Thread.Sleep(10); } // Наличие прутка на стеллаже перед отрезкой
                    MainWindow.AddDebugRecord("Пруток на стеллаже перед отрезкой", 31);
                    MainWindow.AddDebugRecord("Ожидание прутка на линии отрезки", 31);
                    while (DIO.Get_In(S47) || DIO.Get_In(S48) || DIO.Get_In(S49) || DIO.Get_In(S50)) { Thread.Sleep(10); } // Пруток на рольганге перед отрезкой 1 2 3             
                    MainWindow.AddDebugRecord("Пруток на линии отрезки", 31);
                    Thread.Sleep(3000);
                    // Перекладчик на рольганг отрезки в исходное 
                    DIO.ChangeOUT(K11, true);
                    Thread.Sleep(200);
                    DIO.ChangeOUT(K11, false);
                    //
                    Thread.Sleep(2000);
                    // Перекладчик на рольганг отрезки в рабочее 
                    DIO.ChangeOUT(K12, true);
                    Thread.Sleep(200);
                    DIO.ChangeOUT(K12, false);
                    //   
                    Thread.Sleep(1000);
                }

            }

            public void RunSecond()
            {
                MainWindow.AddDebugRecord("Линия отреза старт (2)", 32);
                while (true)
                {
                    // Проверка перекладчика в исходном или нет
                    if (!DIO.Get_In(S55))
                    {
                        DIO.ChangeOUT(K14, true);
                        Thread.Sleep(200);
                        DIO.ChangeOUT(K14, false);
                    }

                    // Проверка сброса в исходном или нет
                    if (!DIO.Get_In(S65))
                    {
                        DIO.ChangeOUT(K25, true);
                        Thread.Sleep(200);
                        DIO.ChangeOUT(K25, false);
                    }

                    // Проверка упора в исходном или нет
                    /*if (!DIO.Get_In(S59))
                    {
                        DIO.ChangeOUT(K19, true);
                        Thread.Sleep(200);
                        DIO.ChangeOUT(K19, false);
                    }*/

                    // Проверка у упора давление 2 в исходном или нет
                    if (!DIO.Get_In(S59))
                    {
                        DIO.ChangeOUT(K19, true);
                        Thread.Sleep(200);
                        DIO.ChangeOUT(K19, false);
                    }

                    // Проверка пилы в исходном или нет
                    if (!DIO.Get_In(S67) && !ThirdLineTrasit)
                    {
                        MainWindow.AddDebugRecord("Пила в рабочем, нужно сделать ее в исходное", 32);
                        while (!DIO.Get_In(S67)) { Thread.Sleep(10); } // Ждем пилы в исходное  
                    }

                    short position = 1;
                    if (DIO.Get_In(S50) && DIO.Get_In(S54)) position = 2;
                    if (DIO.Get_In(S53)) position = 3;
                    if (!DIO.Get_In(S53) && DIO.Get_In(S52)) position = 4;
                    if (ThirdLineTrasit) position = 0;
                    switch (position)
                    {
                        case 1:
                            {
                                MainWindow.AddDebugRecord("Case 1", 32);
                                MainWindow.AddDebugRecord("Ожидание прутка на линии отрезки", 32);
                                while (!DIO.Get_In(S47) && !DIO.Get_In(S48) && !DIO.Get_In(S49)) { Thread.Sleep(10); } // Пруток на рольганге перед отрезкой 1 2 3             
                                MainWindow.AddDebugRecord("Пруток на линии отрезки", 32);
                                DIO.ChangeOUT(K31, true); // Рольганг отрезки 1 вперед 
                                //Thread.Sleep(4000); // Для короткой штанги
                                MainWindow.AddDebugRecord("Ожидание прутка на линии отрезки 2", 32);
                                while (!DIO.Get_In(S54)) { Thread.Sleep(10); } // Пруток на рольганге перед отрезкой 1             
                                MainWindow.AddDebugRecord("Пруток на линии отрезки 2", 32);
                                DIO.ChangeOUT(K35, true); // Рольганг отрезки 2 старт 
                                MainWindow.AddDebugRecord("Ожидание выключения датчика прутка на линии отрезки 2", 32);
                                while (DIO.Get_In(S50)) { Thread.Sleep(10); } // Пруток на рольганге перед отрезкой 2            
                                MainWindow.AddDebugRecord("Пруток на линии отрезки 2", 32);
                                DIO.ChangeOUT(K31, false); // Рольганг отрезки 1 стоп 
                                while (!DIO.Get_In(S53)) { Thread.Sleep(10); } // Пруток по центру рольганга отрезки 2 
                                while (DIO.Get_In(S53)) { Thread.Sleep(10); }

                                Work();

                                break;
                            }
                        case 2:
                            {
                                MainWindow.AddDebugRecord("Case 2", 32);
                                DIO.ChangeOUT(K31, true); // Рольганг отрезки 1 вперед
                                DIO.ChangeOUT(K35, true); // Рольганги отрезки 2 вперед 
                                while (DIO.Get_In(S50)) { Thread.Sleep(10); }
                                DIO.ChangeOUT(K31, false); // Рольганг отрезки 1 стоп

                                while (!DIO.Get_In(S53)) { Thread.Sleep(10); } // Пруток по центру рольганга отрезки 2 
                                while (DIO.Get_In(S53)) { Thread.Sleep(10); }

                                Work();

                                break;
                            }
                        case 3:
                            {
                                MainWindow.AddDebugRecord("Case 3", 32);
                                DIO.ChangeOUT(K35, true);

                                while (DIO.Get_In(S53)) { Thread.Sleep(10); }

                                Work();

                                break;
                            }
                        case 4:
                            {
                                MainWindow.AddDebugRecord("Case 4", 32);

                                DIO.ChangeOUT(K35, true);

                                Work();

                                break;
                            }
                        case 0:
                            {
                                MainWindow.AddDebugRecord("Case 0", 32);

                                TransitMode();

                                break;
                            }
                    }

                }
            }

            private void Work()
            {
                Thread.Sleep(700);

                DIO.ChangeOUT(K35, false); // Рольганги отрезки вперед стоп 2
                //
                // Опустить упор
                DIO.ChangeOUT(K18, true);
                DIO.ChangeOUT(K20, true);
                Thread.Sleep(200);
                DIO.ChangeOUT(K18, false);
                DIO.ChangeOUT(K20, false);
                //                
                if (!DIO.Get_In(S61))
                {
                    MainWindow.AddDebugRecord("Ожидание давление упора в исходное", 32);
                    DIO.ChangeOUT(K20, true);
                    while (!DIO.Get_In(S61)) { Thread.Sleep(10); } // Упор в рабочем 
                    DIO.ChangeOUT(K20, false);
                }

                while (!DIO.Get_In(S60)) { Thread.Sleep(10); } // Упор в рабочем             
                MainWindow.AddDebugRecord("Упор в рабочем положении", 32);
                DIO.ChangeOUT(K36, true); // Рольганги отрезки назад 
                Thread.Sleep(2000);
                DIO.ChangeOUT(K21, true);
                DIO.ChangeOUT(K36, false);
                Thread.Sleep(1500);
                DIO.ChangeOUT(K21, false);
                MainWindow.AddDebugRecord("Ожидание пилы в исходное положение", 32);
                while (!DIO.Get_In(S67)) { Thread.Sleep(10); } // Пила в исходном положении
                MainWindow.AddDebugRecord("Пила в исходном", 32);
                // Пила пуск                             
                DIO.ChangeOUT(K27, true);
                Thread.Sleep(200);
                DIO.ChangeOUT(K27, false);
                //
                // Поднять упор 
                DIO.ChangeOUT(K19, true);
                Thread.Sleep(200);
                DIO.ChangeOUT(K19, false);
                //
                MainWindow.AddDebugRecord("Отрезка прутка", 32);
                MainWindow.AddDebugRecord("Ожидание датчика пила в исходном положении", 32);
                while (DIO.Get_In(S67)) { Thread.Sleep(10); } // Пила в исходном положении    
                while (!DIO.Get_In(S67)) { Thread.Sleep(10); } // Пила в исходном положении            
                MainWindow.AddDebugRecord("Пруток отрезан", 32);

                DIO.ChangeOUT(K36, true); // Рольганги отрезки назад 
                MainWindow.AddDebugRecord("Ожидание датчика пруток на рольганге в начале отрезки", 32);
                while (!DIO.Get_In(S54)) { Thread.Sleep(10); } // Пруток на рольганге в начале отрезкой 2 
                MainWindow.AddDebugRecord("Пруток на линии после отрезки", 32);
                DIO.ChangeOUT(K36, false); // Рольганги отрезки назад стоп
                // Перекладчик с рольганга отрезки в рабочее и сбрасыватель в рабочее
                DIO.ChangeOUT(K13, true);
                DIO.ChangeOUT(K26, true);
                Thread.Sleep(200);
                DIO.ChangeOUT(K13, false);
                DIO.ChangeOUT(K26, false);
                //
                MainWindow.AddDebugRecord("Ожидание датчика сбрасывателя в рабочем", 32);
                while (!DIO.Get_In(S66)) { Thread.Sleep(10); } // Пруток на рольганге в начале отрезкой 2 
                MainWindow.AddDebugRecord("Сбрасываель в рабочем", 32);
                Thread.Sleep(1000);
                // Перекладчик с рольганга отрезки в исходное и сбрасываель в исходное
                DIO.ChangeOUT(K14, true);
                DIO.ChangeOUT(K25, true);
                Thread.Sleep(200);
                DIO.ChangeOUT(K14, false);
                DIO.ChangeOUT(K25, false);
                //
            }

            private void TransitMode()
            {
                if (DIO.Get_In(S56))
                {
                    // Перекладчик с рольганга отрезки в исходное 
                    DIO.ChangeOUT(K14, true);
                    Thread.Sleep(200);
                    DIO.ChangeOUT(K14, false);
                    MainWindow.AddDebugRecord("Ожидание перекладчика в исходное", 32);
                    while (!DIO.Get_In(S55)) { Thread.Sleep(10); } // Перекладчик в рабочем             
                    MainWindow.AddDebugRecord("Перекладчик в исходном", 32);
                }

                short position = 1;
                if (DIO.Get_In(S54)) position = 2;
                if (DIO.Get_In(S52)) position = 3;
                switch (position)
                {

                    case 1:
                        {
                            MainWindow.AddDebugRecord("Ожидание прутка на линии отрезки", 32);
                            while (!DIO.Get_In(S47) && !DIO.Get_In(S48) && !DIO.Get_In(S49)) { Thread.Sleep(10); } // Пруток на рольганге перед отрезкой 1 2 3             
                            MainWindow.AddDebugRecord("Пруток на линии отрезки", 32);
                            DIO.ChangeOUT(K31, true); // Рольганг отрезки 1 вперед 
                            //Thread.Sleep(4000); // Для короткой штанги
                            MainWindow.AddDebugRecord("Ожидание прутка на линии отрезки 2", 32);
                            while (!DIO.Get_In(S54)) { Thread.Sleep(10); } // Пруток на рольганге перед отрезкой в начале 1             
                            MainWindow.AddDebugRecord("Пруток на линии отрезки 2", 32);
                            DIO.ChangeOUT(K35, true); // Рольганг отрезки 2 старт 
                            MainWindow.AddDebugRecord("Ожидание выключения датчика прутка на линии отрезки 2", 32);
                            while (DIO.Get_In(S50)) { Thread.Sleep(10); } // Пруток на рольганге перед отрезкой 2            
                            MainWindow.AddDebugRecord("Пруток на линии отрезки 2", 32);
                            DIO.ChangeOUT(K31, false); // Рольганг отрезки 1 стоп 

                            while (DIO.Get_In(S54)) { Thread.Sleep(10); } // Пруток на рольганге перед в начале отрезкой 1      
                            DIO.ChangeOUT(K35, false); // Рольганг отрезки 2 стоп
                            // Перекладчик с рольганга отрезки в рабочее 
                            DIO.ChangeOUT(K13, true);
                            Thread.Sleep(200);
                            DIO.ChangeOUT(K13, false);
                            //
                            MainWindow.AddDebugRecord("Ожидание датчика перекладчика в рабочем", 32);
                            while (!DIO.Get_In(S56)) { Thread.Sleep(10); } // Пруток на рольганге в начале отрезкой 2 
                            MainWindow.AddDebugRecord("Сбрасываель в рабочем", 32);
                            Thread.Sleep(1000);
                            // Перекладчик с рольганга отрезки в исходное 
                            DIO.ChangeOUT(K14, true);
                            Thread.Sleep(200);
                            DIO.ChangeOUT(K14, false);
                            //

                            break;
                        }

                    case 2:
                        {
                            DIO.ChangeOUT(K31, true); // Рольганг отрезки 1 вперед 
                            //Thread.Sleep(4000); // Для короткой штанги
                            MainWindow.AddDebugRecord("Ожидание прутка на линии отрезки 2", 32);
                            while (!DIO.Get_In(S54)) { Thread.Sleep(10); } // Пруток на рольганге перед отрезкой в начале 1             
                            MainWindow.AddDebugRecord("Пруток на линии отрезки 2", 32);
                            DIO.ChangeOUT(K35, true); // Рольганг отрезки 2 старт 
                            MainWindow.AddDebugRecord("Ожидание выключения датчика прутка на линии отрезки 2", 32);
                            while (DIO.Get_In(S50)) { Thread.Sleep(10); } // Пруток на рольганге перед отрезкой 2            
                            MainWindow.AddDebugRecord("Пруток на линии отрезки 2", 32);
                            DIO.ChangeOUT(K31, false); // Рольганг отрезки 1 стоп 

                            while (DIO.Get_In(S54)) { Thread.Sleep(10); } // Пруток на рольганге перед в начале отрезкой 1      
                            DIO.ChangeOUT(K35, false); // Рольганг отрезки 2 стоп
                            // Перекладчик с рольганга отрезки в рабочее 
                            DIO.ChangeOUT(K13, true);
                            Thread.Sleep(200);
                            DIO.ChangeOUT(K13, false);
                            //
                            MainWindow.AddDebugRecord("Ожидание датчика перекладчика в рабочем", 32);
                            while (!DIO.Get_In(S56)) { Thread.Sleep(10); } // Пруток на рольганге в начале отрезкой 2 
                            MainWindow.AddDebugRecord("Сбрасываель в рабочем", 32);
                            Thread.Sleep(1000);
                            // Перекладчик с рольганга отрезки в исходное 
                            DIO.ChangeOUT(K14, true);
                            Thread.Sleep(200);
                            DIO.ChangeOUT(K14, false);
                            //
                            break;
                        }
                    case 3:
                        {
                            // Перекладчик с рольганга отрезки в рабочее 
                            DIO.ChangeOUT(K13, true);
                            Thread.Sleep(200);
                            DIO.ChangeOUT(K13, false);
                            //
                            MainWindow.AddDebugRecord("Ожидание датчика перекладчика в рабочем", 32);
                            while (!DIO.Get_In(S56)) { Thread.Sleep(10); } // Пруток на рольганге в начале отрезкой 2 
                            MainWindow.AddDebugRecord("Сбрасываель в рабочем", 32);
                            Thread.Sleep(1000);
                            // Перекладчик с рольганга отрезки в исходное 
                            DIO.ChangeOUT(K14, true);
                            Thread.Sleep(200);
                            DIO.ChangeOUT(K14, false);
                            //
                            break;
                        }
                }


            }
        }

        // Плата 1 входа
        public static int S1 = 0; // Стеллаж перед правкой заполнен 1
        public static int S2 = 1; // Стелаж перед правкой заполнен 1
        public static int S3 = 2; // Наличие прутка на стеллажe перед рольгангом правки 1
        public static int S4 = 3; // Пруток на линии правки 1 1
        public static int S5 = 4; // Пруток на линии правки 2 1
        public static int S6 = 5; // Пруток на линии правки 3 1
        public static int S7 = 6; // Пруток перед установкой правки 1
        public static int S8 = 7; // Перекладчик на линию правки в исходном положении 1
        public static int S9 = 8; // Перекладчик на линию правки в рабочем положении 1
        public static int S10 = 9; //
        public static int S11 = 10; //
        public static int S12 = 11; //
        public static int S13 = 12; //
        public static int S14 = 13; //
        public static int S15 = 14; //
        public static int S16 = 15; //
        public static int S17 = 16; //
        public static int S18 = 17; // 
        public static int S19 = 18; //
        public static int S20 = 19; //
        public static int S21 = 20; // Пруток на выходе из установки правки 1
        public static int S22 = 21; // Пруток на рольганге после правки 1
        public static int S23 = 22; // Пруток на рольганге после правки 2
        public static int S24 = 23; // Пруток на рольганге после правки 3
        public static int S25 = 24; // Накопительный стеллаж перед дефектоскопией переполнен 1
        public static int S26 = 25; // Накопительный стеллаж перед дефектоскопией переполнен 1
        public static int S27 = 26; // Перекладчик на накопительный стеллаж в исходном положении 1
        public static int S28 = 27; // Перекладчик на накопительный стеллаж в рабочем положении 1
        public static int S29 = 28; // Наличие прутка на накопительный стелаже 2
        public static int S30 = 29; // Пруток на рольганге перед дефектоскопией 2
        public static int S31 = 30; // Пруток по центру рольганга к дефектоскопии 2
        public static int S32 = 31; // Пруток на рольганге к дефектоскопии 2 2

        // Плата 2 входа
        public static int S33 = 32; // Пруток на рольганге к дефектоскопии 1 2
        public static int S34 = 33; // Перекладчик на рольганг дефектоскопии в рабочем положении 2
        public static int S35 = 34; // Перекладчик на рольганг дефектоскопии в исходном положении 2
        public static int S36 = 35; // Пруток на рольганге после дефектоскопии 3 2
        public static int S37 = 36; // Пруток на рольганге после дефектоскопии 2 2
        public static int S38 = 37; // Пруток на рольганге после дефектоскопии 1 2
        public static int S39 = 38; // Пруток на выходе с установки дефектоскопии 2
        public static int S40 = 39; // Перекладчик брака в рабочем 2
        public static int S41 = 40; // Перекладчик брака в исходном 2
        public static int S42 = 41; // Стелаж перед отрезной заполнен 3
        public static int S43 = 42; // Стелаж перед отрезкой заполнен 3
        public static int S44 = 43; // Наличие прутка на стеллаже перед отрезкой 3
        public static int S45 = 44; // Перекладчик на рольганг отрезки в исходном положении 3
        public static int S46 = 45; // Перекладчик на рольганг отрезки в рабочем положении 3
        public static int S47 = 46; // Пруток на рольганге перед отрезкой 1 1 3
        public static int S48 = 47; // Пруток на центре рольганга отрезки 1 2 3
        public static int S49 = 48; // Пруток на рольганге отрезки 1 3
        public static int S50 = 49; // Пруток на рольганге отрезки перед рольгангом отрезки 2 3
        public static int S51 = 50; // Пруток на рольганге перед отрезкой 2 3
        public static int S52 = 51; // Пруток на рольганге отрезки 2 3
        public static int S53 = 52; // Пруток по центру рольганга отрезки 2 3
        public static int S54 = 53; // Пруток в начале рольганга отрезки 2 3
        public static int S55 = 54; // Перекладчик с линии отрезки в исходном положении 2 3
        public static int S56 = 55; // Перекладчик с линии отрезки в рабочем положении 2 3
        public static int S57 = 56; // Перекладчик на стеллаж перед отрезной в исходном положении 2
        public static int S58 = 57; // Перекладчик на стеллаж перед отрезной в рабочем положении 2
        public static int S59 = 58; // Упор поднят 3  
        public static int S60 = 59; // Упор в рабочем 3 
        public static int S61 = 60; // Пруток в упоре 3
        public static int S65 = 64; // Сбрасыватель в исходном 3
        public static int S66 = 65; // Сбрасыватель в рабочем 3
        public static int S67 = 66; // Пила в исходном положении 3

        //Плата 1 выходы
        public static int K1 = 0; // Перекладчик на рольганг перед правкой в рабочее 1
        public static int K2 = 1; // Перекладчик на рольганг перед правкой в исходное 1
        public static int K3 = 2; //
        public static int K4 = 3; //
        public static int K5 = 4; // Перекладчик с рольганга правки в исходное 1
        public static int K6 = 5; // Перекладчик с рольганга правки в рабочее 1
        public static int K7 = 6; // Перекладчик на рольганг перед дефектоскопией в исходное 2
        public static int K8 = 7; // Перекладчик на стеллаж дефектоскопии в рабочее 2
        public static int K9 = 8; // Перекладчик брака в рабочее 2
        public static int K10 = 9; // Перекладчик брака в исходное 2
        public static int K11 = 10; // Перекладчик на рольганг отрезки в исходное 3
        public static int K12 = 11; // Перекладчик на рольганг отрезки в рабочее 3
        public static int K13 = 12; // Перекладчик с рольганга отрезки в рабочее 3
        public static int K14 = 13; // Перекладчик с рольганга отрезки в исходное 3
        public static int K15 = 14; // Перекладчик после дефектоскопии в рабочее 2
        public static int K16 = 15; // Перекладчик после дефектоскопии в исходное 2
        public static int K18 = 17; // Опустить упор 3
        public static int K19 = 18; // Поднять упор 3 
        public static int K20 = 19; // Исходное положение давления в упоре  
        public static int K21 = 20; // Рабочее положение давления в упоре 3 
        public static int K22 = 21; // Ничего 
        public static int K23 = 22; // Установка правки пуск 1
        public static int K24 = 23; // Установка правки стоп 1
        public static int K25 = 24; // Сбрасыватьль в исходное 3 
        public static int K26 = 25; // Сбрасыватель в рабочее 3
        public static int K27 = 26; // Пила пуск 3
        public static int K29 = 28; // Рольганг перед правкой и после правки вперёд 1
        public static int K30 = 29; // Рольганг перед правкой и после правки назад 1
        public static int K31 = 30; // Рольганги отрезки вперед 3
        public static int K32 = 31; // Рольганги отрезки назад 3

        //Плата 2 выходы
        public static int K33 = 32; // Рольганг после правки вперёд
        public static int K34 = 33; // Рольганг после правки назад
        public static int K35 = 34; // Рольганги отрезки вперед 2
        public static int K36 = 35; // Рольганги отрезки назад 2
    }

}
